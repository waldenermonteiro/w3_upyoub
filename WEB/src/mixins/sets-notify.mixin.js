import { Notify } from 'quasar'
export default {
  methods: {
    setNotifyDanger (message) {
      Notify.create({
        color: 'negative',
        position: 'top',
        message: message,
        icon: 'report_problem'
      })
    },

    setNotifySuccess (message) {
      Notify.create({
        color: 'positive',
        position: 'top',
        message: message,
        icon: 'check_circle'
      })
    },

    setNotifyInfo (message) {
      Notify.create({
        color: 'primary',
        position: 'top',
        message: message,
        icon: 'info'
      })
    }
  }
}
