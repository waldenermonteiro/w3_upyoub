const authRequest = (state) => {
  state.status = 'loading'
}

const authSuccess = (state, resp) => {
  state.status = 'success'
  state.user.name = resp.infos.student_name
  state.user.login = resp.infos.email
  state.token = resp.token
}

const authError = (state) => {
  state.status = 'error'
}

const logout = (state) => {
  state.status = ''
  state.token = ''
}

export default {
  authRequest,
  authSuccess,
  authError,
  logout
}
