import { service } from '../../../plugins/axios'

const login = ({ commit }, user) => {
  return new Promise((resolve, reject) => {
    commit('authRequest')
    service.post('login', user)
      .then(resp => {
        const token = resp.data.token
        const user = resp.data.infos
        window.localStorage.setItem('DATA_SENHA_AVULSA', resp.data)
        window.localStorage.setItem('token_travessia', token)
        window.localStorage.setItem('user_travessia', JSON.stringify(user))
        service.defaults.headers.common['Authorization'] = token
        commit('authSuccess', resp.data)
        resolve(resp)
      })
      .catch(err => {
        commit('authError')
        localStorage.removeItem('token_travessia')
        reject(err)
      })
  })
}
const logout = () => {
  return new Promise((resolve) => {
    window.localStorage.removeItem('token_travessia')
    window.localStorage.removeItem('user_travessia')
    resolve()
  })
}

export default {
  login,
  logout
}
