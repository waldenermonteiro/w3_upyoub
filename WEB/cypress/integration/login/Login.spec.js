/// <reference types="Cypress" />

context('Login de usuário', () => {
    it('Acesso a página de login', () => {
        cy.visit('/')
        cy.wait(2000)
    })
    it('Preencher o campo login', () => {
        cy.get('input[data-cy="login"]').focus().type('3157')
        cy.wait(2000)
    })
    it('Preencher o campo senha', () => {
        cy.get('input[data-cy="password"]').focus().type('3157')
        cy.wait(2000)
    })
    it('Clicar em botão entrar', () => {
        cy.get('button[data-cy="doLogin"]').click()
    })
})